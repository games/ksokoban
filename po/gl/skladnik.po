# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Marce Villarino <mvillarino@kde-espana.es>, 2013.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
# SPDX-FileCopyrightText: 2023, 2024, 2025 Adrián Chaves (Gallaecio)
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-02-28 00:39+0000\n"
"PO-Revision-Date: 2025-03-01 08:32+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.gal>\n"
"Language-Team: Proxecto Trasno (proxecto@trasno.gal)\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Adrián Chaves (Gallaecio)"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "adrian@chaves.gal"

#: InternalCollections.cpp:32
#, kde-format
msgid "Sasquatch"
msgstr "Sasquatch"

#: InternalCollections.cpp:36
#, kde-format
msgid "Mas Sasquatch"
msgstr "Mas Sasquatch"

#: InternalCollections.cpp:40
#, kde-format
msgid "Sasquatch III"
msgstr "Sasquatch III"

#: InternalCollections.cpp:44
#, kde-format
msgid "Microban (easy)"
msgstr "Microban (fácil)"

#: InternalCollections.cpp:48
#, kde-format
msgid "Sasquatch IV"
msgstr "Sasquatch IV"

#: main.cpp:26
#, kde-format
msgid "Skladnik"
msgstr "Skladnik"

#: main.cpp:28
#, kde-format
msgid "The japanese warehouse keeper game"
msgstr "O xogo xaponés de garda de almacén."

#: main.cpp:30
#, kde-format
msgid ""
"(c) 1998 Anders Widell <awl@hem.passagen.se>\n"
"(c) 2012 Lukasz Kalamlacki"
msgstr ""
"© 1998 Anders Widell <awl@hem.passagen.se>\n"
"© 2012 Lukasz Kalamlacki"

#: main.cpp:33
#, kde-format
msgid "Shlomi Fish"
msgstr "Shlomi Fish"

#: main.cpp:34
#, kde-format
msgid "For porting to Qt5/KF5 and doing other cleanups"
msgstr "Migración a Qt 5 e KF 5 e tarefas de limpeza"

#: main.cpp:37
#, kde-format
msgid "Lukasz Kalamlacki"
msgstr "Lukasz Kalamlacki"

#: main.cpp:38
#, kde-format
msgid "For rewriting the original ksokoban game from kde3 to kde4"
msgstr "Reescritura do xogo ksokoban orixinal de KDE 3 para KDE 4"

#: main.cpp:41
#, kde-format
msgid "Anders Widell"
msgstr "Anders Widell"

#: main.cpp:42
#, kde-format
msgid "For writing the original ksokoban game"
msgstr "Autor do xogo ksokoban orixinal"

#: main.cpp:45
#, kde-format
msgid "David W. Skinner"
msgstr "David W. Skinner"

#: main.cpp:46
#, kde-format
msgid "For contributing the Sokoban levels included in this game"
msgstr "Por contribuír cos niveis de Sokoban que se inclúen co xogo."

#: main.cpp:57
#, kde-format
msgid "Level collection file to load."
msgstr "Ficheiro de colección de niveis para cargar."

#: MainWindow.cpp:38
#, kde-format
msgctxt "@title:menu"
msgid "Level Collection"
msgstr "Colección de niveis"

#: MainWindow.cpp:97
#, kde-format
msgctxt "@action"
msgid "Load Levels…"
msgstr "Cargar niveis…"

#: MainWindow.cpp:101
#, kde-format
msgctxt "@action"
msgid "Next Level"
msgstr "Seguinte nivel"

#: MainWindow.cpp:106
#, kde-format
msgctxt "@action"
msgid "Previous Level"
msgstr "Nivel anterior"

#: MainWindow.cpp:111
#, kde-format
msgctxt "@action"
msgid "Restart Level"
msgstr "Reiniciar o nivel"

#: MainWindow.cpp:124
#, kde-format
msgctxt "@item animation speed"
msgid "Slow"
msgstr "Lenta"

#: MainWindow.cpp:129
#, kde-format
msgctxt "@item animation speed"
msgid "Medium"
msgstr "Media"

#: MainWindow.cpp:134
#, kde-format
msgctxt "@item animation speed"
msgid "Fast"
msgstr "Rápida"

#: MainWindow.cpp:139
#, kde-format
msgctxt "@item animation speed"
msgid "Off"
msgstr "Desactivada"

#: MainWindow.cpp:179 MainWindow.cpp:193
#, kde-format
msgctxt "@item bookmark entry"
msgid "(unused)"
msgstr "(sen usar)"

#: MainWindow.cpp:229
#, kde-format
msgctxt "@item bookmark entry"
msgid "(invalid)"
msgstr "(incorrecto)"

#: MainWindow.cpp:288
#, kde-format
msgctxt "@title:window"
msgid "Load Levels from a File"
msgstr "Cargar niveis dun ficheiro"

#: MainWindow.cpp:323
#, kde-format
msgctxt "@info"
msgid "No levels found in file."
msgstr "Non se atoparon niveis no ficheiro."

#: PlayField.cpp:91
#, kde-format
msgctxt "@label"
msgid "Level:"
msgstr "Nivel:"

#: PlayField.cpp:100
#, kde-format
msgctxt "@label"
msgid "Steps:"
msgstr "Pasos:"

#: PlayField.cpp:109
#, kde-format
msgctxt "@label"
msgid "Pushes:"
msgstr "Empurróns:"

#: PlayField.cpp:270
#, kde-format
msgctxt "@info"
msgid "Level completed."
msgstr "Completouse o nivel."

#: PlayField.cpp:582
#, kde-format
msgid "This is the last level in the current collection."
msgstr "Este é o último nivel da colección actual."

#: PlayField.cpp:586
#, kde-format
msgid "You have not completed this level yet."
msgstr "Aínda non completaches este nivel."

#: PlayField.cpp:598
#, kde-format
msgid "This is the first level in the current collection."
msgstr "Este é o primeiro nivel da colección actual."

#: PlayField.cpp:674
#, kde-format
msgid "Bookmarks for external levels is not implemented yet."
msgstr "Os marcadores aínda non están dispoñíbeis para niveis externos."

#: PlayField.cpp:698
#, kde-format
msgid "This level is broken."
msgstr "Este nivel está danado."

#. i18n: ectx: Menu (game)
#: skladnikui.rc:6
#, kde-format
msgid "&Game"
msgstr "&Partida"

#. i18n: ectx: Menu (animation_speed)
#: skladnikui.rc:20
#, kde-format
msgid "&Animation"
msgstr "&Animacións"

#. i18n: ectx: Menu (bookmarks_menu)
#: skladnikui.rc:27
#, kde-format
msgid "&Bookmarks"
msgstr "&Marcadores"

#. i18n: ectx: Menu (set_bookmarks)
#: skladnikui.rc:29
#, kde-format
msgid "&Set Bookmark"
msgstr "&Definir un marcador"

#. i18n: ectx: Menu (goto_bookmarks)
#: skladnikui.rc:42
#, kde-format
msgid "&Go to Bookmark"
msgstr "&Ir a un marcador"

#. i18n: ectx: ToolBar (mainToolBar)
#: skladnikui.rc:57
#, kde-format
msgid "Main Toolbar"
msgstr "Barra de ferramentas principal"

#~ msgid "KSokoban"
#~ msgstr "KSokoban"

#~ msgid "[file]"
#~ msgstr "[ficheiro]"

#~ msgid "&Undo"
#~ msgstr "&Desfacer"

#~ msgid "&Redo"
#~ msgstr "&Refacer"

#~ msgid "&Quit"
#~ msgstr "&Saír"

#~ msgid "Sasquatch V"
#~ msgstr "Sasquatch V"

#~ msgid "Mas Microban (easy)"
#~ msgstr "Mas Microban (fácil)"

#~ msgid "Sasquatch VI"
#~ msgstr "Sasquatch VI"

#~ msgid "LOMA"
#~ msgstr "LOMA"

#~ msgid "Sasquatch VII"
#~ msgstr "Sasquatch VII"

#~ msgid "(c) 1998-2001  Anders Widell"
#~ msgstr "© 1998-2001 Anders Widell"
